<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $genres = [
            'Action',
            'Drama',
            'Fantasy',
            'Sci-fi',
            'Horror',
            'Mystery',
            'Romance',
          ];
          foreach ($genres as $genre) {
            DB::table('genres')->insert([
              'genre' => $genre,
            ]);
    }
}}
