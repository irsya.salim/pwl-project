@extends('layouts.app')

{{-- set title --}}
@section('title', 'Fiction World')

@section('content')

    <section class="pt-5">
        <div class="container">
            <div class="row pt-5">
                <div class="col">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col d-flex justify-content-end">
                    <form action="/" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" size="50" placeholder="Cari buku disini..."
                                name="search" />
                            <div class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container py-4 vh-100">
            <div class="row">
                @foreach ($bukus as $Buku)
                    <div class="col" style="margin-bottom: 20px">
                        <div class="card " style="width: 15rem; height: auto;">
                            <div class="card-body" style=" display: flex; flex-direction: column; justify-content: space-between;">
                                <div class="frame" style="width:207px; height:311px">
                                    <img src="img/{{ $Buku->cover }}" class="card-img-top pb-3" style=" max-width: 100%; max-height: 100%; object-fit: cover; object-position: center;" alt=""
                                        srcset="">
                                </div>
                                <h5 class="card-title text-truncate">{{ $Buku->title }}</h5>
                                <p class="card-text m-0" style="max-width: 100%; color:rgb(88, 83, 83);" >{{ $Buku->author }}</p>
                                <p class="card-text" style="max-width: 100%; color:darkcyan"> Rp{{ number_format($Buku->price, 2, ',', '.')}} </p>
                                <a href="/detail/{{ $Buku->id }}" class="btn btn-primary" style="margin-top: auto;">Lihat</a>
                            </div>
                        </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection

@push('after-style')
@endpush
