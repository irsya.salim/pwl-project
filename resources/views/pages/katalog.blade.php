@extends('layouts.app')

{{-- set title --}}
@section('title', 'Fiction World')

@section('content')

    <section class="pt-5">
        <div class="container-fluid pt-5">
            <div class="row m-4">
                <div class="col-sm-1">
                    {{-- --}}
                </div>
                <div class="col-sm-3 text-center">
                    <img src="../img/{{ $buku->cover }}" width="200" height="300">
                </div>
                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-8 col-sm-7">
                            <p> <span class="fs-6" style="color: grey">{{ $buku->author }}</span> <br>
                                <span class="fs-3" style="font-weight:500">{{ $buku->title }}</span> <br>
                                <span class="fs-5" style="">Genre Buku: {{ $buku->genre->genre }}</span>
                            </p>
                            <span class="fs-3" style="color:darkcyan">Rp{{ number_format($buku->price, 2, ',', '.') }}</span>
                            <hr width="100%" />
                            <b style="font-size: large;">Sinopsis Buku</b>
                            <p style="font-family:'Times New Roman'; font-size: 17px; text-align: justify;">
                                {{ $buku->synopsis }}</p>
                        </div>  
                        <div class="col-4 col-sm-5">
                            <h2>Ingin beli berapa?</h2>
                            <form action="/prosesPemesanan" method="post" id="purchaseForm">
                                @csrf
                                <div class="input-container">
                                    <input id="pmentdate" type="hidden" name="pmentdate"
                                        value="{{ now()->format('Y-m-d') }}">
                                </div>
                                <script>
                                    const pmentDateInput = document.getElementById('pmentdate');
                                    pmentDateInput.value = new Date().toISOString().slice(0, 10);
                                </script>
                                <label for="text" class="form-label"
                                    Style="font-size: 19px; font-style: Segoe ui;">Jumlah Buku</label>
                                <input type="number" id="text" class="form-control mb-3" style="width:370px"
                                    name="quantity" id="quantity" required>
                                <input type="hidden" name="buku_id" value="{{ $buku->id }}">
                                <input type="hidden" name="user_id" value="{{ Session::get('user_id') }}">
                                <button type="button" onclick="confirmOrder()" class="btn btn-primary" style="text-align: center;">Beli
                                    Sekarang</button>
                                <button type="submit" id="submitButton" style="display: none;">Beli
                                    Sekarang</button>
                            </form>
                            <div id="customAlert" class="alert" style="display: none;">
                                <p>Apakah Anda yakin ingin membeli buku ini?</p>
                                <button onclick="confirmPurchase()">Ya</button>
                                <button onclick="hideAlert()">Tidak</button>
                            </div>
                            <div class="overlay"></div>
                            <script>
                                function confirmOrder() {
                                    document.getElementById('customAlert').style.display = 'block';
                                    document.querySelector('.overlay').style.display = 'block'

                                }
                            
                                function hideAlert() {
                                    document.getElementById('customAlert').style.display = 'none';
                                    document.querySelector('.overlay').style.display = 'none'
                                }
                            
                                function confirmPurchase() {
                                    document.getElementById('purchaseForm').submit();
                                }
                            </script>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1">
                    {{-- --}}
                </div>
            </div>
        </div>
    </section>
    <style>
        .overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            display: none;
        }
.alert {
    position: fixed;
    z-index: 1000;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: #d4edda;
    border-color: #d4edda;
    color: black;
    padding: 15px;
    border-radius: 5px;
    max-width: 300px;
    text-align: center;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
}

.alert-success {
    background-color: #d4edda;
    border-color: #c3e6cb;
    color: #155724;
}

.alert-danger {
    background-color: #f8d7da;
    border-color: #f5c6cb;
    color: #721c24;
}

.alert p {
    margin: 0;
}

.alert button {
    margin-top: 10px;
    padding: 5px 15px;
    background-color: #0d6efd;
    color: #fff;
    border: none;
    border-radius: 3px;
    cursor: pointer;
}

.alert button:hover {
    background-color: #0056b3;
}
    </style>
@endsection

@push('after-style')
@endpush
