<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function indexAction()
    {
        $genre = Genre::find(1);
        if ($genre && $genre->buku) {
            $data = [
                'bukus' => $genre->buku,
                'title' => 'Genre Action'
            ];
            return view('pages.genre', $data);
        } else {
            return redirect('/');
        }
    }

    public function indexDrama()
    {
        $genre = Genre::find(2);
        if ($genre && $genre->buku) {
            $data = [
                'bukus' => $genre->buku,
                'title' => 'Genre Drama'
            ];
            return view('pages.genre', $data);
        } else {
            return redirect('/');
        }
    }


    public function indexFantasy()
    {
        $genre = Genre::find(3);
        if ($genre && $genre->buku) {
            $data = [
                'bukus' => $genre->buku,
                'title' => 'Genre Fantasy'
            ];
            return view('pages.genre', $data);
        } else {
            return redirect('/');
        }
    }

    public function indexScifi()
    {
        $genre = Genre::find(4);
        if ($genre && $genre->buku) {
            $data = [
                'bukus' => $genre->buku,
                'title' => 'Genre Sci-fi'
            ];
            return view('pages.genre', $data);
        } else {
            return redirect('/');
        }
    }

    public function indexHorror()
    {
        $genre = Genre::find(5);
        if ($genre && $genre->buku) {
            $data = [
                'bukus' => $genre->buku,
                'title' => 'Genre Horror'
            ];
            return view('pages.genre', $data);
        } else {
            return redirect('/');
        }
    }

    public function indexMystery()
    {
        $genre = Genre::find(6);
        if ($genre && $genre->buku) {
            $data = [
                'bukus' => $genre->buku,
                'title' => 'Genre Mystery'
            ];
            return view('pages.genre', $data);
        } else {
            return redirect('/');
        }
    }

    public function indexRomance()
    {
        $genre = Genre::find(7);
        if ($genre && $genre->buku) {
            $data = [
                'bukus' => $genre->buku,
                'title' => 'Genre Romance'
            ];
            return view('pages.genre', $data);
        } else {
            return redirect('/');
        }
    }
}
